/// variables ///

let result = document.getElementById("result");
let form = document.getElementById("add-user-form");
let nameInput = document.getElementById("name");
let emailInput = document.getElementById("email");

///database settings  adding ///

var firebaseConfig = {
  apiKey: "AIzaSyB0QgW0I6V6CJr6waaz9ht-st0iEdxyLpA", ///firebase hesabında login altında real time data base ye bağlı !!!
  authDomain: "login-a6c4a.firebaseapp.com",
  databaseURL: "https://login-a6c4a.firebaseio.com",
  projectId: "login-a6c4a",
  storageBucket: "login-a6c4a.appspot.com",
  messagingSenderId: "787138742896",
  appId: "1:787138742896:web:39d6e02e1054f1181382b5",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });

///db collection called get method to local///

// db.collection("users").get().then((snapshot)=> {
// snapshot.docs.forEach(myData => {

//   firebaseDataHendler(myData)
// });
// });

/// create element and add data ///
function firebaseDataHendler(myData) {
  const article = document.createElement("article");
  const name = document.createElement("h3");
  const text = document.createElement("h5");
  const year = document.createElement("span");
  const time = document.createElement("span");
  const button = document.createElement("button");
  button.setAttribute("class", "d-block mx-auto btn-sm btn-danger my-3");
  button.textContent = "delete"
  article.setAttribute("data-id", myData.id);
  article.setAttribute("class", "twit-container");
  name.textContent = myData.data().name;
  text.textContent = myData.data().email;
  year.textContent = myData.data().year;
  time.textContent = myData.data().time;
  article.appendChild(name);
  article.appendChild(text);
  article.appendChild(year);
  article.appendChild(time);
  article.appendChild(button);

  result.appendChild(article);

  button.addEventListener("click",function (e) {
    e.stopPropagation()
    let id = e.target.parentElement.getAttribute('data-id');
    db.collection("users").doc(id).delete()
  })
}

/// send form data function ///

form.addEventListener("submit", (e) => {
  e.preventDefault();
  db.collection("users").add({
    name: nameInput.value,
    email: emailInput.value,
    year : new Date().toLocaleDateString(),
    time: new Date().toLocaleTimeString()
  });
  nameInput.value = "";
  emailInput.value = "";
});

///realtime data db collection called  ///

db.collection("users")
  .orderBy("name")
  .onSnapshot((snapshot) => {
    let changes = snapshot.docChanges();
    console.log(changes);

    changes.forEach((change) => {
      if (change.type == "added") {
        firebaseDataHendler(change.doc);
      } else if (change.type == "removed") {
        let li = result.querySelector("[data-id=" + change.doc.id + "]");
        result.removeChild(li);
      }
    });
  });
